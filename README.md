# Inter Process Communication

## Examples
As shown in these examples, you can specify the types of objects that will be 
serialized and deserialized when communicating between server and client.  

It's important to note that you only need to specify the type that the server or
client (whichever you are initializing) will *_receive_*. It's not important to
explicitly declare the type that it will send because the send function only needs
an object type.  

It's also important to note that the server and client don't have to use the same
type. This is illustrated in the 3rd example. It's just necessary that the server
and client are careful in only sending messages that the recipient can read.

### Sending and receiving strings
```csharp
public class Program
{
    static void Main(string[] args)
    {
        var server = new SocketServer<string>();
        server.ComHandlerFactory = new SocketComHandlerFactory();
        server.OnClientConnected += Server_OnClientConnected;
        server.Listen();


        var client = new SocketClient<string>();
        client.ComHandlerFactory = new SocketComHandlerFactory();
        client.OnDataReceived += Client_OnDataReceived;
        client.Listen();

        while (true)
            client.Send(Console.ReadLine());
    }

    private static void Server_OnClientConnected(object sender, ClientConnectedEventArgs<string> e)
    {
        Console.WriteLine($"[SERVER] Client has connected");
        e.SocketComHandler.OnDataReceived += SocketComHandler_OnDataReceived;
        e.SocketComHandler.OnDisconnected += SocketComHandler_OnDisconnected;
    }

    private static void SocketComHandler_OnDisconnected(object sender, EventArgs e)
    {
        Console.WriteLine($"[SERVER] Client has disconnected");
    }

    private static void SocketComHandler_OnDataReceived(object sender, DataReceivedEventArgs<string> e)
    {

        if (sender is SocketComHandler<string> socketClientListener)
        {
            var response = "";
            if (e.ErrorMsg != null)
            {

                Console.WriteLine($"[SERVER] Error occurred: {e.ErrorMsg}");
                response = $"An error occurred while processing your request: {e.ErrorMsg}";
            }
            else
            {
                Console.WriteLine($"[SERVER] Received {e.Data}");
                response = $"Hey thanks for sending me: {e.Data}";
            }
            socketClientListener.Send(response);
        }

    }

    private static void Client_OnDataReceived(object sender, DataReceivedEventArgs<string> e)
    {
        if (e.ErrorMsg != null)
            Console.WriteLine($"[CLIENT] Error occurred: {e.ErrorMsg}");
        else
            Console.WriteLine($"[CLIENT] Received {e.Data}");
    }
}
```

### Sending and receiving byte arrays
```csharp
public class Program
{
    static void Main(string[] args)
    {
        var server = new SocketServer<byte[]>();
        server.ComHandlerFactory = new SocketComHandlerFactory();
        server.OnClientConnected += Server_OnClientConnected;
        server.Listen();


        var client = new SocketClient<byte[]>();
        client.ComHandlerFactory = new SocketComHandlerFactory();
        client.OnDataReceived += Client_OnDataReceived;
        client.Listen();

        while (true)
            client.Send(Console.ReadLine());

    }

    private static void Server_OnClientConnected(object sender, ClientConnectedEventArgs<byte[]> e)
    {
        Console.WriteLine($"[SERVER] Client has connected");
        e.SocketComHandler.OnDataReceived += SocketComHandler_OnDataReceived;
        e.SocketComHandler.OnDisconnected += SocketComHandler_OnDisconnected;
    }

    private static void SocketComHandler_OnDisconnected(object sender, EventArgs e)
    {
        Console.WriteLine($"[SERVER] Client has disconnected");
    }

    private static void SocketComHandler_OnDataReceived(object sender, DataReceivedEventArgs<byte[]> e)
    {

        if (sender is SocketComHandler<byte[]> socketClientListener)
        {
            if (e.ErrorMsg != null)
            {
                Console.WriteLine($"[SERVER] Error occurred: {e.ErrorMsg}");
            }
            else
            {
                Console.WriteLine($"[SERVER] Received {e.Data.Length} bytes");
            }
            socketClientListener.Send(e.Data);
        }
    }

    private static void Client_OnDataReceived(object sender, DataReceivedEventArgs<byte[]> e)
    {
        if (e.ErrorMsg != null)
            Console.WriteLine($"[CLIENT] Error occurred: {e.ErrorMsg}");
        else
            Console.WriteLine($"[CLIENT] Received {e.Data.Length} bytes");
    }
}
```

### Sending and receiving custom objects
```csharp
public class Program
{
    static void Main(string[] args)
    {
        var server = new SocketServer<SocketRequest>();
        server.ComHandlerFactory = new SocketComHandlerFactory();
        server.OnClientConnected += Server_OnClientConnected;
        server.Listen();


        var client = new SocketClient<SocketResponse>();
        client.ComHandlerFactory = new SocketComHandlerFactory();
        client.OnDataReceived += Client_OnDataReceived;
        client.Listen();

        while (true)
            client.Send(new SocketRequest() {
                Id = Guid.NewGuid().ToString(),
                Request = Console.ReadLine()
            });
    }

    private static void Server_OnClientConnected(object sender, ClientConnectedEventArgs<SocketRequest> e)
    {
        e.SocketComHandler.OnDataReceived += SocketComHandler_OnDataReceived;
        e.SocketComHandler.OnDisconnected += SocketComHandler_OnDisconnected;
    }

    private static void SocketComHandler_OnDisconnected(object sender, EventArgs e)
    {
        Console.WriteLine($"[SERVER] Client has disconnected");
    }

    private static void SocketComHandler_OnDataReceived(object sender, DataReceivedEventArgs<SocketRequest> e)
    {

        if (sender is SocketComHandler<SocketRequest> socketClientListener)
        {
            var response = new SocketResponse();
            if (e.ErrorMsg != null)
            {

                Console.WriteLine($"[SERVER] Error occurred: {e.ErrorMsg}");
                response.Status = "Error";
                response.Response = $"An error occurred while processing your request: {e.ErrorMsg}";
            }
            else
            {
                Console.WriteLine($"[SERVER] Id: {e.Data.Id}, Request: {e.Data.Request}");
                response.Id = e.Data.Id;
                response.Status = "Success";
                response.Response = $"Hey thanks for sending me: {e.Data.Request}";
            }
            socketClientListener.Send(response);
        }

    }

    private static void Client_OnDataReceived(object sender, DataReceivedEventArgs<SocketResponse> e)
    {
        if (e.ErrorMsg != null)
            Console.WriteLine($"[CLIENT] Error occurred: {e.ErrorMsg}");
        else
            Console.WriteLine($"[CLIENT] Status: {e.Data.Status}, Response: {e.Data.Response}");
    }
}
```