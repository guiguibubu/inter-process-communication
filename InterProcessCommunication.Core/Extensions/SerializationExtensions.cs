﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace InterProcessCommunication.Core.Extensions
{
    public static class SerializationExtensions
    {
        public static string XmlSerializeToString(this object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                writer.NewLine = "";
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T XmlDeserializeFromString<T>(this string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }

        public static string JsonSerializeToString(this object objectInstance)
        {
            var serializer = new JsonSerializer();
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                //writer.NewLine = "";
                serializer.Serialize(writer, objectInstance, objectInstance.GetType());
            }

            return sb.ToString();
        }

        public static T JsonDeserializeFromStream<T>(this Stream stream)
        {
            return (T)JsonDeserializeFromStream(stream, typeof(T));
        }

        public static object JsonDeserializeFromStream(this Stream stream, Type type)
        {
            var serializer = new JsonSerializer();

            using (var sr = new StreamReader(stream, Encoding.UTF8, true, 1024, true))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize(jsonTextReader, type);
            }
        }

        public static T JsonDeserializeFromString<T>(this string objectData)
        {
            return (T)JsonDeserializeFromString(objectData, typeof(T));
        }

        public static object JsonDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new JsonSerializer();
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader, type);
            }

            return result;
        }
    }
}
