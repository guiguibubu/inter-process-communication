﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;

namespace InterProcessCommunication.Core.Sockets
{
    // T is the Type that the messages received should be deserialized to
    public interface ISocketComHandler<T>
    {
        event EventHandler<DataReceivedEventArgs<T>> OnDataReceived;
        event EventHandler OnDisconnected;

        void Send(object message);
        void Handle();
    }
}
