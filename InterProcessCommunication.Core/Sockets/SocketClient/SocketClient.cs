﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using InterProcessCommunication.Core.Sockets.SocketComHandler;

namespace InterProcessCommunication.Core.Sockets
{
    public class SocketClient<T>: ISocketClient<T>
    {
        private TcpClient client;

        public event EventHandler<DataReceivedEventArgs<T>> OnDataReceived;

        private ISocketComHandler<T> socketComHandler;

        private ISocketComHandlerFactory _comHandlerFactory;
        public ISocketComHandlerFactory ComHandlerFactory { get => _comHandlerFactory; set { _comHandlerFactory = value; InitComHandler(); } }

        public SocketClient()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var host = appSettings["SocketServerAddr"];
            var port = Int32.Parse(appSettings["SocketServerPort"]);
            Init(host, port);
        }

        public SocketClient(string host, int port)
        {
            Init(host, port);
        }

        private void Init(string host, int port)
        {
            try
            {
                client = new TcpClient(host, port);
                InitComHandler();
            }
            catch (Exception e)
            {
                Console.WriteLine($"[SocketClient] Exception thrown in constructor: {e}");
            }
        }

        private void InitComHandler()
        {
            if (ComHandlerFactory != null)
            {
                socketComHandler = ComHandlerFactory.Create<T>(client);
                socketComHandler.OnDataReceived += SocketClient_OnDataReceived;
                socketComHandler.OnDisconnected += SocketComHandler_OnDisconnected;
            }
        }

        private void SocketComHandler_OnDisconnected(object sender, EventArgs e)
        {
            Console.WriteLine("[SocketClient] The connection to the server has been closed");
        }

        private void SocketClient_OnDataReceived(object sender, DataReceivedEventArgs<T> e)
        {
            OnDataReceived?.Invoke(sender, e);
        }

        public void Send(object message)
        {
            socketComHandler.Send(message);
        }

        public void Listen()
        {
            socketComHandler.Handle();
        }
    }
}
